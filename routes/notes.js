var express = require('express');
var router = express.Router();
var noteController = require("../controllers/noteController");

/* GET home page. */
router.get('/', noteController.index);
router.get('/add',noteController.add);
router.get('/delete/:id', noteController.deleteNote);
router.get('/update/:id', noteController.getNoteForUpdate);
router.get('/:id', noteController.getNoteById);

/**
 * Post routes
 */
router.post('/add', noteController.addNote);
router.post('/update/:id', noteController.updateNote);

module.exports = router;

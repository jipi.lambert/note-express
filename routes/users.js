let express = require('express');
let router = express.Router();
let usersController = require('../controllers/usersController')

router.get('/', usersController.index);
router.get('/login', usersController.login);

router.post('/register', usersController.registerUser);
router.post('/login', usersController.loginUser);

module.exports = router;
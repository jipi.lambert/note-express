var express = require('express');
var router = express.Router();
var noteController = require("../controllers/noteController");

/* GET home page. */
router.get('/', async (req,res,next) => {
    res.render("index", {title:'note taking app'});
});

module.exports = router;

const { NoteModel } = require('../models/noteModel');

exports.index = async (req, res, next) => {
    try {
        let document = await NoteModel.find({});
        res.render('notes/index', { title: 'Notes', notes: document });
    } catch ( err ) {
        console.log(err);
        res.status(500).send("Internal Server error Occured");
    }
}

exports.getNoteById = async (req, res, next) => {
    try {
        let document = await getNoteById(req.params.id);
        res.render('notes/note', { title: 'Notes', note: document });
    } catch ( err ) {
        console.log(err);
        res.status(500).send("Internal Server error Occured");
    }
}

exports.getNoteForUpdate = async (req, res, next) => {
    try {
        let document = await getNoteById(req.params.id);
        res.render('notes/edit', { note: document })
    } catch ( err ) {
        console.log(err);
        res.status(500).send("Internal Server error Occured");
    }
}

exports.add = (req, res, next) => {
    res.render("notes/add");
}

exports.addNote = async (req, res, next) => {
    let data = new NoteModel(req.body);
    data.save(function (err, data) {
        if (err) console.error(err);
        res.redirect('/notes')
    })
}

exports.updateNote =  async (req, res, next) => {
    try {
        await NoteModel.updateOne({ _id: req.params.id }, req.body)
        res.redirect('/notes');
    } catch ( err ) {
        console.log(err);
        res.status(500).send("Internal Server error Occured");
    }
}

exports.deleteNote = async (req, res, next) => {
    try {
        await NoteModel.findByIdAndRemove(req.params.id)
        res.redirect('/notes');
    } catch ( err ) {
        console.log(err);
        res.status(500).send("Internal Server error Occured");
    }
}

const getNoteById = async (id) =>{
    return await NoteModel.findById(id);
}
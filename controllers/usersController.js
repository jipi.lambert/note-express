const { UserModel } = require('../models/usersModel');
const bcrypt = require('bcrypt');

exports.index = (req,res,next) =>{
    res.render('users/add');
}

exports.login = (req,res,next) =>{
    res.render('users/login');
}

exports.registerUser = async (req,res,next) =>{
    try{
        const hashed_password = await bcrypt.hash(req.body.password, 10);
        const user = new UserModel({
            name: req.body.name, 
            email: req.body.email,
            password: hashed_password
        });

        user.save((err, document)=>{
            if (err) res.render('register', {error:err});
            res.render('users/login', {data:document});
        });
    } catch(err) {
        console.log(err);
        res.status(500).send("Internal Server error Occured");
    }
}

exports.loginUser = async (req, res,next) =>{
    try{
        const user = await UserModel.findOne({email:req.body.email});
        if(user){
            let compare = await bcrypt.compare(req.body.password, user.password); 
            if(compare){
                res.render('users/user', {title:"mon utilisateur favori", username: user.name})
            } else {
                res.render('error',{message:"Mauvais nom d'utilisateur ou mot de passe."});
            }
        } else {
            res.render('error',{message:"Mauvais nom d'utilisateur ou mot de passe."});
        }
    } catch (err) {
        console.log(err);
        res.status(500).send("Internal Server error Occured");
    }
}